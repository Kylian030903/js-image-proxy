import fastify from "fastify";
import fastifyCors from "fastify-cors";
import axios from "axios";
import sharp from "sharp";

/**
 * @typedef QueryString
 * @type {object}
 * @property {string} width
 * @property {string} height
 * @property {"webp" | "png" | "jpg" | "jpeg"} format
 * @property {"flip" | "flop" | "flip,flop"} orientation
 */

/**
 * @typedef ResponseTypes
 * @type {object}
 * @property {string} webp
 * @property {string} png
 * @property {string} jpg
 * @property {string} jpeg
 */

const server = fastify();

server.register(fastifyCors);

/**
 * @constant imageRoute
 * @type {import("fastify").RouteOptions}
 */
const imageRoute = {
  method: "GET",
  url: "/*",
  schema: {
    params: {
      image: {
        type: "string",
      },
    },
    querystring: {
      width: {
        type: "string",
      },
      height: {
        type: "string",
      },
      format: {
        type: "string",
      },
      orientation: {
        type: "string",
      },
    },
  },
  async handler(request, reply) {
    const baseImage = request.url.slice(1).split("?")[0];

    /**
     * @constant query
     * @type {QueryString}
     */
    const { width, height, format, orientation } = request.query;

    if (!baseImage) {
      reply.send(400).send("Bad request: no image provided.");
      return;
    }

    /**
     * @constant formats
     * @type {Array<"webp" | "png" | "jpg" | "jpeg">}
     */
    const formats = ["webp", "png", "jpg", "jpeg"];

    if (format && !formats.some((f) => format === f)) {
      reply
        .code(400)
        .send(
          `Bad request: supported image formats are ${formats.join(", ")}.`
        );
      return;
    }
    /**
     * @var imageRequest
     * @type {import("axios").AxiosResponse}
     */
    let imageRequest;

    try {
      imageRequest = await axios.get(baseImage, {
        responseType: "arraybuffer",
      });
    } catch {
      reply.code(500).send("Failed to fetch provided image.");
    }

    /**
     * @constant image
     * @type {import("sharp").Sharp}
     */
    const image = sharp(Buffer.from(imageRequest.data, "binary"));
    /**
     * @constant imageMetadata
     * @type {import("sharp").Metadata}
     */
    const imageMetadata = await image.metadata();

    image.resize({
      width: width ? parseInt(width) : imageMetadata.width,
      height: height ? parseInt(height) : imageMetadata.height,
    });

    switch (orientation) {
      case "flip":
        image.flip(true);
        break;

      case "flop":
        image.flop(true);
        break;

      case "flip,flop":
        image.flip(true);
        image.flop(true);
        break;

      default:
        break;
    }

    switch (format) {
      case "webp":
        image.webp();
        break;

      case "png":
        image.png();
        break;

      case "jpg":
        image.jpeg();
        break;

      case "jpeg":
        image.jpeg();
        break;

      default:
        image.webp();
        break;
    }

    /**
     * @constant responseTypes
     * @type {ResponseTypes}
     */
    const responseTypes = {
      webp: "image/webp",
      png: "image/png",
      jpg: "image/jpeg",
      jpeg: "image/jpeg",
    };

    reply.header("Content-Type", responseTypes[format ? format : "webp"]);
    reply.send(await image.toBuffer());
  },
};

server.route(imageRoute);

server.listen(4000, "0.0.0.0", (error) => {
  if (error) {
    throw error;
  }

  console.info("Server listening on port 4000.");
});
